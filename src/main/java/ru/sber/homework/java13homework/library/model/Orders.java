package ru.sber.homework.java13homework.library.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name="orders")
@SequenceGenerator(name="default_gen", sequenceName = "orders_seq", allocationSize = 1)
public class Orders extends GenericModel {
    @ManyToOne
    @JoinColumn(name = "users_id",foreignKey = @ForeignKey(name="FC_ORDERS_USERS"))
    private Users users;

    @ManyToOne
    @JoinColumn(name = "Films_id",foreignKey = @ForeignKey(name="FC_ORDERS_FILMS"))
    private Films films;

    @Column(name="rent_date",nullable = false)
    private Date rentDate;

    @Column(name="rent_period", nullable = false)
    private Integer rentPeriod;

    @Column(name="purchase" , nullable = false)
    private boolean purchase;
}
