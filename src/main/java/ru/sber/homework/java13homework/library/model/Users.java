package ru.sber.homework.java13homework.library.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="users")
@SequenceGenerator(name = "default_Generator", sequenceName = "users_seq", allocationSize = 1)
public class Users extends GenericModel{

    @Column(name="login", nullable = false)
    private String login;

    @Column(name="password", nullable = false)
    private String password;

    @Column(name="first_name", nullable = false)
    private String firstName;

    @Column(name="last_name", nullable = false)
    private String lastName;

    @Column(name="middle_name")
    private String middleName;

    @Column(name="birth_date", nullable = false)
    private Date birthDate;

    @Column(name = "phone", nullable = false)
    private Integer phone;

    @Column(name = "address")
    private String address;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "created_when", nullable = false)
    private Date createdWhen;

    @ManyToOne
    @JoinColumn(name="role_id",nullable = false,foreignKey = @ForeignKey(name="FC_USER_ROLES"))
    private Role role;


}
