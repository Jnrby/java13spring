package ru.sber.homework.java13homework.library.model;

public enum Genre {
    FANTASY("Фантастика"),
    WESTERN("Вестерн"),
    COMEDY("Комедия"),
    ACTION("Боевик");

    private final String genreDisplayValue;

    Genre(String text) {
        this.genreDisplayValue=text;
    }

    public String getGenreDisplayValue() {
        return genreDisplayValue;
    }
}
