package ru.sber.homework.java13homework.library.model;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Entity
@Setter
@Getter
@Table(name="directors")
@NoArgsConstructor
@SequenceGenerator(name = "default_Generator", sequenceName = "directors_seq", allocationSize = 1)
public class Directors extends GenericModel{


    @Column(name = "directors_fio", nullable = false)
    private String directorsFIO;

    @Column(name="position")
    private String position;

    @ManyToMany(mappedBy = "directors")
    private Set<Films> films;

}
