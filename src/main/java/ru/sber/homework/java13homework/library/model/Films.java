package ru.sber.homework.java13homework.library.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@Entity
@Table(name="films")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_Generator", sequenceName = "films_seq", allocationSize = 1)
public class Films extends GenericModel{


    @Column(name="title", nullable = false)
    private String title;

    @Column(name="premier_year", nullable = false)
    private Date premierYear;

    @Column(name="county", nullable = false)
    private String country;

    @Column(name="genre", nullable = false)
    @Enumerated
    private Genre genre;
    @ManyToMany
    @JoinTable(name = "films_directors",
            joinColumns = @JoinColumn(name="film_id"), foreignKey = @ForeignKey(name="FK_FILMS_DIRECTORS"),
            inverseJoinColumns = @JoinColumn(name="director_id"), inverseForeignKey = @ForeignKey(name ="FK_DIRECTORS_FILMS"))
    private Set<Directors> directors;

}
