package ru.sber.homework.java13homework.dbexample;

import jakarta.persistence.SequenceGenerator;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.sber.homework.java13homework.dbexample.dao.UserDaoBean;
import ru.sber.homework.java13homework.dbexample.model.Users;

import java.util.Arrays;

@Getter
@Setter
@ToString
@SpringBootApplication
public class UserDAO implements CommandLineRunner {
    private UserDaoBean userDaoBean;

    @Autowired
    public void setUserDaoBean(UserDaoBean userDaoBean) {
        this.userDaoBean = userDaoBean;
    }

    public static void main(String[] args) {
        SpringApplication.run(UserDAO.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("===========================================");
//        Users users= new Users("Andrey", "Fedin", "9263130466", "juner@yandex.ru", "Java для чайников");
//        userDaoBean.addNewUser(users);
        System.out.println(userDaoBean.bookList(userDaoBean.gettingBooksByPhone("9263130455")));

    }
}
