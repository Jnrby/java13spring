package ru.sber.homework.java13homework.dbexample.dao;


import org.springframework.stereotype.Component;
import ru.sber.homework.java13homework.dbexample.model.Books;
import ru.sber.homework.java13homework.dbexample.model.Users;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserDaoBean {
    private final Connection connection;

    public UserDaoBean(Connection connection) {
        this.connection = connection;
    }

    public void addNewUser(Users newUsers) throws SQLException {
        PreparedStatement insertQuery = connection.prepareStatement("insert into dz6_users (lastname, firstname, birthday, phone, email, rented_books)" + "values (?,?,now(),?,?,?)");
        insertQuery.setString(1, newUsers.getLastName());
        insertQuery.setString(2, newUsers.getFirstName());
        insertQuery.setString(3, newUsers.getPhone());
        insertQuery.setString(4, newUsers.getEmail());
        insertQuery.setString(5, newUsers.getRentedBooks());
        insertQuery.executeUpdate();
    }

    public String[] gettingBooksByPhone(String phone) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement("select * from dz6_users where phone=?");
        selectQuery.setString(1, phone);
        ResultSet resultSet = selectQuery.executeQuery();
        resultSet.next();
        String[] rentedBooks = resultSet.getString("rented_books").split(";");
        return rentedBooks;
    }

    public List<Books> bookList(String[] userBook) throws SQLException {
        List<Books> infoBook = new ArrayList<>();
        for (String str : userBook) {
            String sql = "select * from dz6_books where title=?";
            PreparedStatement selectQuery = connection.prepareStatement(sql);
            selectQuery.setString(1, str);
            ResultSet result = selectQuery.executeQuery();
            Books book = new Books();
            while (result.next()) {
                book.setBookId(result.getInt("id"));
                book.setBookTitle(result.getString("title"));
                book.setBookAuthor(result.getString("author"));
                book.setDateAdded(result.getDate("date_added"));
            }
            infoBook.add(book);
        }
        return infoBook;

    }


}
