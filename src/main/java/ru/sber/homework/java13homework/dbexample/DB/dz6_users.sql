create table dz76_users
(
    id           serial primary key,
    lastname     varchar(30)   not null,
    firstname    varchar(30)   not null,
    birthday     date          not null,
    phone        varchar(20)   not null,
    email        varchar(50)   not null,
    rented_books varchar(1000) not null
);